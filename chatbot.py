#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple Bot to reply to Telegram messages
# This program is dedicated to the public domain under the CC0 license.
"""
This Bot uses the Updater class to handle the bot.
First, a few callback functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import sys

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)
token=sys.argv[1]

import logging

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

START,etat_menu_sorties, propositions_resto, etat_liste_resto, etat_info_resto, etat_top_3_bars, etat_top_3_clubs, etat_top_3_musees, retour = range(9)


def start(bot, update):
    reply_keyboard = [['Sorties', 'Restaurants']]

    update.message.reply_text(
        'Salut! Bienvenue sur mon RoxBot. Je peux t\'aider à trouver des restos et sorties sur Genève ! '
        'Envoie /cancel à tout moment pour arrêter de me parler.\n\n'
        'Es-tu intéressé(e) par des sorties ou des restaurants ?',
                            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return START

######################################------------------------------------------------------ SORTIES

def afficher_menu_sorties(bot, update):
    user = update.message.from_user
    reply_keyboard = [['Musées', 'Bars', 'Clubs']]
    update.message.reply_text('Je vois ! Sélectionne ce qui t\'intéresse, '
                              'ou envoie /skip si tu ne veux pas répondre à cette question.',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return etat_menu_sorties

def top_3_musees(bot, update):
    user = update.message.from_user
    update.message.reply_text('Voici notre top 3 des musées de Genève:' 
                              'Le Mamco' 
                              'La Maison Tavel' 
                              'le Musée d\'Histoire Naturelle',
                              reply_markup=ReplyKeyboardRemove())

    return retour

def top_3_bars(bot, update):
    user = update.message.from_user
    update.message.reply_text('Voici notre top 3 des bars de Genève:' 
                              'Le Kraken Bar'
                              'Mr. Pickwick Pub' 
                              'le Lady Godiva Pub',
                              reply_markup=ReplyKeyboardRemove())

    return retour

def top_3_clubs(bot, update):
    user = update.message.from_user
    update.message.reply_text('Voici notre top 3 des Clubs de Genève:' 
                              'La Gravière'
                              'L\'Usine' 
                              'Le Zoo',
                              reply_markup=ReplyKeyboardRemove())

    return retour

######################################------------------------------------------------------ RESTAURANTS

def afficher_propositions_resto(bot, update):
    user = update.message.from_user
    reply_keyboard = [['Italien', 'Japonais', 'Chinois', 'Français', 'Suisse', 'Espagnol']]
    update.message.reply_text('Sélectionne le type de nourriture que tu souhaites, '
                              'ou envoie /skip si tu ne veux pas répondre à cette question.',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return etat_liste_resto

def afficher_liste_resto(bot, update):
    user = update.message.from_user
    reply_keyboard = [['Le trinquet', 'Laz Nillo', 'Restaurant le 15','Les philosophes', 'Le Socrate', 'McDo']]
    update.message.reply_text('ok! Maintenant, choisis le restaurant qui te plaît le plus, '
                              'ou envoie /skip si tu ne veux pas répondre à cette question.',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return etat_info_resto


def info_resto(bot, update):
    user = update.message.from_user
    update.message.reply_text('837 avis– 4 étoiles – Attestation d\'Excellence – Adresse: Goutama no. 3, GE 80571, Suisse',
                              reply_markup=ReplyKeyboardRemove())

    return retour

######################################------------------------------------------------------

def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Oh... tant pis. Au plaisir de te revoir !',
                              reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)

######################################------------------------------------------------------ MAIN

def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(token)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],

        states={
            START:[
                RegexHandler('^(Sorties)$', afficher_menu_sorties ),
                RegexHandler('^(Restaurants)$', afficher_propositions_resto)
            ],

            afficher_menu_sorties: [
                RegexHandler('^(Musées)$', top_3_musees),
                RegexHandler('^(Bars)$', top_3_bars),
                RegexHandler('^(Clubs)$', top_3_clubs),
            ],

            retour: [
                RegexHandler('^(Retour)$', afficher_menu_sorties),
            ],

            etat_liste_resto: [
                 RegexHandler('^(Italien)$', afficher_liste_resto),
                 RegexHandler('^(Japonais)$', afficher_liste_resto),
                 RegexHandler('^(Chinois)$', afficher_liste_resto),
                 RegexHandler('^(Français)$', afficher_liste_resto),
                 RegexHandler('^(Suisse)$', afficher_liste_resto),
                 RegexHandler('^(Espagnol)$', afficher_liste_resto),
          ],

            afficher_liste_resto: [
                RegexHandler('^(Le trinquet)$', info_resto),
                RegexHandler('^(Laz Nillo)$',info_resto),
                RegexHandler('^(Restaurant le 15)$', info_resto),
                RegexHandler('^(Les philosophes)$', info_resto),
                RegexHandler('^(Le Socrate)$', info_resto),
                RegexHandler('^(McDo)$', info_resto),
            ],

        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    dp.add_handler(conv_handler)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()